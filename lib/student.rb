class Student
attr_reader :first_name , :last_name
  def initialize(first,last)
    @first_name = first
    @last_name = last
    @courses=[]
  end

  def name
    return @first_name + ' ' + @last_name
  end

  def courses
    return @courses
  end

  def enroll(course)
    if @courses.include?(course)
      return
    end
    @courses.push(course)
    course.students.push(self)
  end

  def course_load
    output=Hash.new(0)
    @courses.each do |course|
      output[course.department]+=course.credits
    end
    output
  end

end
